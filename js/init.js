$(document).ready(function (){
   //Cuando hago click al icono de lupa javascript hace click al icono de las barras
   $('.searchinput').click(function(e){
      e.preventDefault();
      $('.buscador').trigger("click");
      return false;
   });
   //Cuando hago click en cualquier link del mega menu header o footer detengo cualquier evento
   $('.col-megamenu a').click(function(e){
      e.preventDefault();
      return false;
   });
   //Cuando clikc en cualquier enlace de noticias me llevara a la noticia
   $('.linknoticia a').click(function(){
      window.location.href = '/noticia.html';
      return false;
   });
   
});